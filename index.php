<?php
require 'sort.php';
require 'pattern.php';
require 'countsort.php';

    // sorting array soal no 1-A
    $str = array(12,9,30,"A","M",99,82,"J","N","B","b","a");
    echo "soal 1-A => </br>";
    echo "Soal : ".json_encode($str)."</br>";
    echo "Jawaban : ".sorting($str);
    echo "</br>";
    echo "</br>";
    
    // count string soal no 1-B
    $word = "aaaaaa";
    $str2 = "aa";
    echo "soal 1-B => </br>";
    echo "word : ".$word." => pattern : ".$str2."</br>";
    echo "Jawaban : ".pattern_count($word, $str2);
    echo "</br>";

    echo "</br>";
    // soal no 1-C
    echo "soal 1-C => </br>";
    $str3 = "MasyaAllah";
    echo "Soal : ".$str3."</br>";
    echo "Jawaban : ".countsort($str3);
?>