<?php

    function countsort($str){
        $pieces = explode(" ", $str);
        $v = '';
        foreach($pieces as $p){
            $v .= $p;
        }
        $s = str_split($v);
        
        $count = array_count_values($s);
        
        array_multisort(array_keys($count), SORT_NATURAL | SORT_FLAG_CASE, $count);
        $out = array_values($count);
        return json_encode($count);
    }
?>