<?php
    function pattern_count($str, $word){
        $pos=-1;  // start at -1 so that first iteration uses $pos of 0 as starting offset
        $tally=0;
        while(($pos=strpos($str,$word,++$pos))!==false){++$tally;}
        return $tally;
    } 
?>